/* Eneriemonitor
 Z�hlen der Pulse von den Energiemessger�ten in der Hauptverteilung(HV) Gesamt
 und Unterverteilung (UV) K�che und Serverschrank
 Anzeige der Messergebnisse als Html WEB Server



K�hler Thomas 2019.06.02

enth�lt code aus den Standart Beispielen
Web Server; WIFI MANAGER; PUBSUB MQTT; ArduinoJson
*/



#include <ESP8266mDNS.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include "config.h"
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <PubSubClient.h>

#include <WiFiUdp.h>
#include <FS.h>
//OTA
#include <ArduinoOTA.h>
// ESP 8266
#include <ESP8266WiFi.h>

extern "C" {
#include "user_interface.h" //to set the hostname
}

#ifndef DEBUG
#define DEBUG false  // Change to 'false' to disable all serial output.
#endif  // DEBUG




unsigned long conTime;

char Buffer[10];
boolean   INT_STATE;

struct sensor {
	String category;
	String Name;
	String unit;
	unsigned int value;
};


WiFiServer server(80);
WiFiClient espClient;
PubSubClient MQTTclient(espClient);




unsigned long Pulse[NumberUsedInput];
unsigned long PulseLast[NumberUsedInput];
unsigned long Energy[NumberUsedInput];
unsigned long Power[NumberUsedInput];
unsigned long PowerTrigger[NumberUsedInput];
sensor Sensor[(NumberUsedInput + 1) * 2];
//time
unsigned long Time[NumberUsedInput];
unsigned long ValidTime[NumberUsedInput];
unsigned long LastTime[NumberUsedInput];
// 
unsigned long LastTriggerTime[sizeof TriggerTime];
unsigned long NextPulseSave;
String LastMqttData;
boolean ToogleLED;









void setPowerSensor(byte InNr)
{

	Power[InNr] = (double(3600000000) / ((double(ValidTime[InNr]) * double(PulsePerkWh[InNr]))));




	Sensor[InNr] = { categoryPower, SensorenName[InNr],unitPower,Power[InNr] };
	Serial.println(Power[InNr]);
	Serial.println(ValidTime[InNr]);


	MqttWrite(InNr);
if (NumberUsedInput<4){return;}
	if (Power[1] + Power[2] < Power[0]) {
		Power[3] = Power[0] - Power[1] - Power[2];
		Serial.println(Power[3]);
	}
	else {
		Power[3] = 0;
	}
	Sensor[3] = { categoryPower, SensorenName[3],unitPower,Power[3] };

	MqttWrite(3);
}


void  setEnergySensor(byte InNr)
{
	Energy[InNr - NumberUsedInput] = int(Pulse[InNr - NumberUsedInput] - PulseLast[InNr - NumberUsedInput]) * 1000 / PulsePerkWh[InNr - NumberUsedInput];
	PulseLast[InNr - NumberUsedInput] = Pulse[InNr - NumberUsedInput];

	if (InNr == 3 + 4) {
		if (Energy[1] + Energy[2] < Energy[0]) {
			Energy[3] = Energy[0] - Energy[1] - Energy[2];
		}
		else {
			Energy[3] = 0;
		}
	}

	Sensor[InNr] = { categoryEnergy, SensorenName[InNr - NumberUsedInput],unitEnergy,Energy[InNr - NumberUsedInput] };

	MqttWrite(InNr);

}





#pragma region configFile

void config() {

	char buf[100];
	Serial.println("Configuration portal requested");

	WiFiManagerParameter p_mqtt("mqtt", "Mqtt", MqttServer, 100);
	mqtt_mainTopic.toCharArray(buf, sizeof mqtt_mainTopic + 2);
	WiFiManagerParameter p_maintopic("MainTopic", "MainTopic", buf, sizeof mqtt_mainTopic + 2);
	WiFiManagerParameter p_hint("<small>*Hint: if you want to reuse the currently active WiFi credentials, leave SSID and Password fields empty</small>");
	WiFiManagerParameter p_hint_mqtt1("<small>Mqtt Server Adresse</small>");
	WiFiManagerParameter p_hint_mqtt2("<small>MainTopic</small>");
	WiFiManagerParameter p_hint_host("<small>MainTopic</small>");
	// Initialize WiFIManager
	WiFiManager wifiManager;

	//add all parameters here
	wifiManager.addParameter(&p_hint);
	wifiManager.addParameter(&p_mqtt);
	wifiManager.addParameter(&p_hint_mqtt1);
	wifiManager.addParameter(&p_maintopic);
	wifiManager.addParameter(&p_hint_mqtt2);

	// Sets timeout in seconds until configuration portal gets turned off.
	// If not specified device will remain in configuration mode until
	// switched off via webserver or device is restarted.
	// wifiManager.setConfigPortalTimeout(600);

	// It starts an access point 
	// and goes into a blocking loop awaiting configuration.
	// Once the user leaves the portal with the exit button
	// processing will continue
	if (!wifiManager.autoConnect()) {
		Serial.println("Not connected to WiFi but continuing anyway.");
	}
	else {
		// If you get here you have connected to the WiFi
		Serial.println("Connected...yeey :)");
	}
	//strcpy(MqttServer, p_mqtt.getValue());

	mqtt_mainTopic = p_maintopic.getValue();

	// Writing JSON config file to flash for next boot
	writeConfigFile();




	Serial.println("CONFIG ENDE");

}

bool readConfigFile() {
	// this opens the config file in read-mode
	File f = SPIFFS.open(CONFIG_FILE, "r");

	if (!f) {
		Serial.println("Configuration file not found");
		return false;
	}
	else {
		// we could open the file
		size_t size = f.size();
		// Allocate a buffer to store contents of the file.
		std::unique_ptr<char[]> buf9(new char[size]);

		// Read and store file contents in buf
		f.readBytes(buf9.get(), size);
		// Closing file
		f.close();
		// Using dynamic JSON buffer which is not the recommended memory model, but anyway
		// See https://github.com/bblanchon/ArduinoJson/wiki/Memory%20model
		DynamicJsonBuffer jsonBuffer;
		// Parse JSON string
		JsonObject& json = jsonBuffer.parseObject(buf9.get());
		// Test if parsing succeeds.
		if (!json.success()) {
			Serial.println("JSON parseObject() failed");
			return false;
		}
		json.printTo(Serial);

		// Parse all config file parameters, override 
		// local config variables with parsed values
		if (json.containsKey("mqttServerAdress")) {
			char str1[100];
		//	strcpy(MqttServer, json["mqttServerAdress"]);


		}
		if (json.containsKey("mqttMainTopic")) {
			char str1[32];
			strcpy(str1, json["mqttMainTopic"]);
			String str(str1);
			mqtt_mainTopic = str;
		}



	}
	Serial.println("\nConfig file was successfully parsed");
	return true;
}





bool writeConfigFile() {
	Serial.println("Saving config file");
	DynamicJsonBuffer jsonBuffer;
	JsonObject& json = jsonBuffer.createObject();

	// JSONify local configuration parameters
	json["mqttServerAdress"] = MqttServer;
	json["mqttMainTopic"] = mqtt_mainTopic;

	// Open file for writing
	File f = SPIFFS.open(CONFIG_FILE, "w");
	if (!f) {
		Serial.println("Failed to open config file for writing");
		return false;
	}

	json.prettyPrintTo(Serial);
	// Write data to file and close it
	json.printTo(f);
	f.close();

	Serial.println("\nConfig file was successfully saved");
	return true;
}
bool readPulseFile() {
	// this opens the config file in read-mode
	File f = SPIFFS.open(PULSE_FILE, "r");

	if (!f) {
		Serial.println("Configuration file not found");
		return false;
	}
	else {
		// we could open the file
		size_t size = f.size();
		// Allocate a buffer to store contents of the file.
		std::unique_ptr<char[]> buf9(new char[size]);

		// Read and store file contents in buf
		f.readBytes(buf9.get(), size);
		// Closing file
		f.close();
		// Using dynamic JSON buffer which is not the recommended memory model, but anyway
		// See https://github.com/bblanchon/ArduinoJson/wiki/Memory%20model
		DynamicJsonBuffer jsonBuffer;
		// Parse JSON string
		JsonObject& json = jsonBuffer.parseObject(buf9.get());
		// Test if parsing succeeds.
		if (!json.success()) {
			Serial.println("JSON parseObject() failed");
			return false;
		}
		json.printTo(Serial);




		// Parse all config file parameters, override 
		// local config variables with parsed values
		for (int i = 0; i < NumberUsedInput; i++) {
			if (json.containsKey("P_IN" + String(i))) {
				char str1[40];
				strcpy(str1, json["P_IN" + String(i)]);
				String str(str1);
				Pulse[i] = atol(str1);

				Serial.println(Pulse[i]);
			}
		}
	}

	Serial.println("\nConfig file was successfully parsed");
	return true;
}

bool writePulseFile() {
	Serial.println("Saving config file");
	DynamicJsonBuffer jsonBuffer;
	JsonObject& json = jsonBuffer.createObject();

	// JSONify local configuration parameters

	for (int i = 0; i < NumberUsedInput; i++) {
		json["P_IN" + String(i)] = String(Pulse[i]);
	}

	// Open file for writing
	File f = SPIFFS.open(PULSE_FILE, "w");
	if (!f) {
		Serial.println("Failed to open config file for writing");
		return false;
	}

	json.prettyPrintTo(Serial);
	// Write data to file and close it
	json.printTo(f);
	f.close();

	Serial.println("\nConfig file was successfully saved");
	return true;
}

#pragma endregion

#pragma region Interrupt
//*****************Beginn Interuppt Routinen************************
void ICACHE_RAM_ATTR Interupt(byte InNr) {

	unsigned long tmp = millis();
	unsigned long diff = tmp - LastTime[InNr];
	if (diff > 50) {
		Time[InNr] = diff;
		ValidTime[InNr] = diff;
		Pulse[InNr] += 1;
		LastTime[InNr] = tmp;
		ToogleLED = true;
		Serial.println(InNr);
	}
}

void ICACHE_RAM_ATTR Interupt_IN0() {

	Interupt(0);
}

void ICACHE_RAM_ATTR Interupt_IN1() {

  if (NumberUsedInput > 1){
	Interupt(1);
  }
}

void ICACHE_RAM_ATTR Interupt_IN2() {
if (NumberUsedInput > 2){
  Interupt(2);
  }
}

//***************** ENDE Interuppt Routinen ************************
#pragma endregion

#pragma region MQTT
//**************** Begin Mqtt Funktionen ************************
void reconnect() {
	//Serial.print("Attempting MQTT connection...");
	// Attempt to connect
	if (MQTTclient.connect(HostName))
	{
		//Serial.println("connected");
	}
	else
	{
		Serial.println("failed, rc=");
		Serial.print(MQTTclient.state());
		delay(2000);
	}
}

void MqttWrite(byte SensorNr) {


	String Name = mqtt_mainTopic + "/" + Sensor[SensorNr].category + "/" + Sensor[SensorNr].Name;

	String Data;

	Data += mqtt_mainTopic + ",";
	Data += "category=" + Sensor[SensorNr].category + ",";
	Data += "sensor=" + Sensor[SensorNr].Name + ",";
	Data += "unit=" + Sensor[SensorNr].unit + " ";
	Data += "value=" + String(Sensor[SensorNr].value);

	char bufTopic[100];
	char bufData[100];
	unsigned int  size = Name.length();
	Name.toCharArray(bufTopic, size + 2);
	size = Data.length();
	Data.toCharArray(bufData, size + 2);
	Serial.println(bufTopic);
	Serial.println(bufData);
	LastMqttData = String(bufTopic) + "/" + String(bufData);
	MQTTclient.publish(bufTopic, bufData);
	delay(10);

}

char* leadingZero(char* input) {
	for (int i = 0; i < 10; i++) {

		if (input[i] == ' ') {
			input[i] = 0;
		}

	}

	return input;
}
//***************** ENDE Mqtt Funktionen ************************
#pragma endregion

#pragma region Webserver
//*****************Begin WEB Server Funktionen************************




void PrintEnergie(WiFiClient client, String Ueberschrift, unsigned long Pulse, unsigned long power_value, unsigned long energy_value, unsigned long Pulstime, unsigned long ValidPulstime) {
	//Anzeige der Energie und Leistungsdaten

  //tmpd Gesamt verbrauchte Energie
	double tmpd = double(double(energy_value) / double(1000));
	double tmpd1 = double(double(TriggerTime[1]) / double(60000));
	client.print("<h2>" + Ueberschrift + " ENEGRGY Last " + tmpd1 + " min : " + tmpd + " kWh " + power_value + " W" + "</h2>");
	client.println("<BR>");
	client.print("Pulse: ");
	client.print(Pulse);
	client.print(" | Zeit seit letzten Puls: ");
	tmpd = double((double(millis()) - double(Pulstime)) / double(1000));
	client.print(tmpd);
	client.print("s | Valid PulsZeit: ");
	tmpd = double(double(ValidPulstime) / double(1000));
	client.print(tmpd);
	client.print("s");
	client.println("<BR>");
	client.println("<BR>");

	client.println("<BR>");
}

void serverHtml() {
#define BUFS 100
	char clientline[BUFS];
	int index = 0;

	WiFiClient client1 = server.available();
	if (client1) {
		// an http request ends with a blank line
		boolean current_line_is_blank = true;

		// reset the input buffer
		index = 0;

		while (client1.connected()) {
			if (client1.available()) {
				char c = client1.read();

				// If it isn't a new line, add the character to the buffer
				if (c != '\n' && c != '\r') {
					clientline[index] = c;
					index++;
					// are we too big for the buffer? start tossing out data
					if (index >= BUFS)
						index = BUFS - 1;

					// continue to read more data!
					continue;
				}

				// got a \n or \r new line, which means the string is done
				clientline[index] = 0;

				// Print it out for debugging
				Serial.println(clientline);

				// Look for substring such as a request to get the root file
				if (strstr(clientline, "GET / ") != 0) {

					// send a standard http response header
					client1.println("HTTP/1.1 200 OK");
					client1.println("Content-Type: text/html");
					client1.println("Connection: close");  // the connection will be closed after completion of the response
					client1.println("Refresh: 3");  // refresh the page automatically every 30 sec
					client1.println();
					client1.println("<!DOCTYPE HTML>");
					client1.println("<html><body>");
					// print all the files, use a helper to keep it clean
					client1.println("<h1>HAL Webserver Energiemessung</h1>");
					for (int i = 0; i < NumberUsedInput; i++) {
						PrintEnergie(client1, SensorenName[i], Pulse[i], Sensor[i].value, Sensor[i + NumberUsedInput].value, LastTime[i], ValidTime[i]);
					}
					client1.println("IP: ");
					client1.println(WiFi.localIP());
					client1.println("<BR>");
					client1.print("LAST MQTT TOPIC: ");
					client1.println(LastMqttData);
					client1.println("</body></html>");
					client1.println();
					client1.println();
				}
				else if (strstr(clientline, "GET /") != 0) {

				}
				else {
					// everything else is a 404
					Serial.println("WEB404");
					client1.println("HTTP/1.1 404 Not Found");
					client1.println("Content-Type: text/html");
					client1.println();
					client1.println("<h2>File Not Found!</h2>");
				}
				break;
			}
			// give the web browser time to receive the data
			delay(1);
			client1.stop();
		}

	}
}
//*****************Ende WEB Server Funktionen************************
#pragma endregion

#pragma region allgemeine fuction
//*****************Begin Allgemeine Funktionen************************

void checkEnergie() {
	for (int i = 0; i < NumberUsedInput; i++) {
		setEnergySensor(i + NumberUsedInput);
	}
}
void checkPower() {

	for (int i = 0; i < NumberUsedInput; i++) {
    Serial.print("checkPower");
     Serial.println(i);
		if (Pulse[i] >= PowerTrigger[i]) {
			PowerTrigger[i] = Pulse[i] + 1;
			setPowerSensor(i);
		}
	}

}

boolean checkTimeTrigger(byte TriggerNr) {
	if (millis() - LastTriggerTime[TriggerNr] >= TriggerTime[TriggerNr]) {
		LastTriggerTime[TriggerNr] = millis();
		Serial.println(String(TriggerTime[TriggerNr]) + "s Trigger");
		return true;
	}
	return false;
}


void SavePulse() {
	if (Pulse[PulseSaveIN] > NextPulseSave) {
		Serial.println("PULSE_SAVE");
		writePulseFile();
		NextPulseSave = Pulse[PulseSaveIN] + PulseSave;
	}
}

//***************** ENDE Allgemeine Funktionen************************

#pragma endregion

void setup() {




	// Open serial communications and wait for port to open:
	Serial.begin(115200);
	while (!Serial) {
		; // wait for serial port to connect. Needed for native USB port only
	}
	bool result = SPIFFS.begin();
	delay(10);
	Serial.println("SPIFFS opened: " + result);

	if (!readConfigFile()) {
		Serial.println("Failed to read configuration file, using default values");

	}
	WiFi.hostname(HostName);

	if (!readPulseFile()) {
		Serial.println("Failed to read configuration file, using default values");

	}

	for (int i = 0; i < NumberUsedInput; i++) {

		PowerTrigger[i] = Pulse[i] + 2;

	}
	NextPulseSave = Pulse[PulseSaveIN] + PulseSave;
	config();
	//OTA
	ArduinoOTA.setHostname(HostName);
	ArduinoOTA.onStart([]() {
		String type;
		if (ArduinoOTA.getCommand() == U_FLASH) {
			type = "sketch";
		}
		else { // U_SPIFFS
			type = "filesystem";
		}

		// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
		Serial.println("Start updating " + type);
	});
	ArduinoOTA.onEnd([]() {
		Serial.println("\nEnd");
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
	});
	ArduinoOTA.onError([](ota_error_t error) {
		Serial.printf("Error[%u]: ", error);
		if (error == OTA_AUTH_ERROR) {
			Serial.println("Auth Failed");
		}
		else if (error == OTA_BEGIN_ERROR) {
			Serial.println("Begin Failed");
		}
		else if (error == OTA_CONNECT_ERROR) {
			Serial.println("Connect Failed");
		}
		else if (error == OTA_RECEIVE_ERROR) {
			Serial.println("Receive Failed");
		}
		else if (error == OTA_END_ERROR) {
			Serial.println("End Failed");
		}
	});
	ArduinoOTA.begin();

	//Interupt Aktiv setzen


	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(IN0, INPUT);
	attachInterrupt(digitalPinToInterrupt(IN0), Interupt_IN0, RISING);
	pinMode(IN1, INPUT);
	attachInterrupt(digitalPinToInterrupt(IN1), Interupt_IN1, RISING);
	pinMode(IN2, INPUT);
	attachInterrupt(digitalPinToInterrupt(IN2), Interupt_IN2, RISING);


	//MQTT Start//
	// start the Ethernet connection and the server:

	server.begin();
	Serial.print("server is at ");
	Serial.println(WiFi.localIP());
	MQTTclient.setServer(MqttServer, 1883);

}

void loop() {

	if (!MQTTclient.connected()) { reconnect(); }
	MQTTclient.loop();

	ArduinoOTA.handle();

	SavePulse();


	// Refresh DATA
	if (checkTimeTrigger(0)) { checkPower(); };
	if (checkTimeTrigger(1)) { checkEnergie(); };

	// do server Voodoo
	serverHtml();
	if (ToogleLED) {
		digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
		ToogleLED = false;
	}





	//	
	delay(50);

}
