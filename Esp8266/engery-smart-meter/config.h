#ifndef config_H
#define config_H

// IO Config PIN
const int IN0 = D5;
const int IN1 = D6;
const int IN2 = D7;

//Hostname
const char HostName[] = "Energie03";

//  mqtt
char MqttServer[100] = "hautomation01.hacklabor.de";
String mqtt_mainTopic = "sensors";

// Array Nr == IN Nr. 
const int NumberUsedInput = 1;
const char *SensorenName[]  = { "LABOR"};

//Sensor Resolution Pulse je kWh

const int PulsePerkWh[] = { 1000 };


const char categoryEnergy[] = "energy";
const char unitEnergy[] = "Wattstunde";

const char categoryPower[] = "power";
const char unitPower[] = "Watt";
//TriggerTime ArayNr 0=10s 1=15min 
unsigned long TriggerTime[] = { 10000, 15*60000 };


// every PulseSave INpulse save pulse to SPIFFS
unsigned long PulseSave =1000;
const int PulseSaveIN = 0;
// SAVE File Name
const char* CONFIG_FILE = "/config.json";
const char* PULSE_FILE = "/pulse.json";


#endif
