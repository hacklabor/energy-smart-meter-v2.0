# Energy-smart-meter V2.0 HAL

## installierte Messgeräte

Hauptverteilung Messen des gesamt Energieverbrauchs

[DRT751DE MID M16 - zugelassen bis 2024 Handbuch](http://www.bg-etech.de/download/manual/DRT751DE.pdf) 
 
Unterverteilung 

* Messgerät 1 Serverschrank
* Messgerät 2 Küche
* weitere?
 
[DRS155DC-V3 Handbuch](http://bg-etech.de/download/manual/DRS155DCV3.pdf)

## Meßart

die Messgeräte sind fest in der Haupt bzw Unterververteilung eingebaut

* lmpulsausgang S0 nach DIN EN 62053-31 - Kl. A
* potenzialfrei durch einen Optokoppler,
* max. 27V DC / 20mA

## Auflösung

* 1Wh/Pulse (DRT751DE)
* 0,5Wh/Pulse (DRS155DC-V3)

## Auswerteelektronik

### ESP 8266 Wemos mini
[d1_mini_v2.3.0](https://wiki.wemos.cc/products:retired:d1_mini_v2.3.0)
### Wemos mini Protype shield
[d1_mini_shields:protoboard_shield](https://wiki.wemos.cc/products:d1_mini_shields:protoboard_shield)

### Layout
![Platine](/pictures/energy-smart-metertV20_Steckplatine.png)
![Platine](/pictures/energy-smart-meterV20_Schaltplan.png)
## Formeln
### Leistung
P=U*I= W[Ws]/t[s] = 1Pulse/(t<sub>2</sub>-t<sub>1</sub>)

### Min Zeit zwischen zwei Pulsen bei 2000Pulse/kWh

P<sub>max</sub>= U*I =230Vx16A=3680W = 7300Pulse
7300Pulse/3600=2,04Pulse/s=0,489s/Pulse

Wertetabelle

![100Pulse](/calc/Wertetabelle1000PulsejekWh.PNG)
![200Pulse](/calc/Wertetabelle2000PulsejekWh.PNG)
[/calc/Rechentool.ods](/calc/Rechentool.ods "Tool")



